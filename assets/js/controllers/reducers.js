'use strict';
import { createReducer } from 'redux-orm';
import {combineReducers} from 'redux';
import ExerciseRunner from './exerciseRunner/reducers';


const rootReducer = combineReducers({
    exerciseRunner: ExerciseRunner
});

export default rootReducer;