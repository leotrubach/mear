export const ExerciseActions = {
    EXERCISE_RUNNER_OPEN: 'EXERCISE_RUNNER_OPEN',
    KINDS_LOADING: 'KINDS_LOADING',
    KINDS_RECEIVED: 'KINDS_RECEIVED',
    EXERCISE_CANCEL: 'EXERCISE_CANCEL',
    SET_EXERCISE_REQUEST_SENT: 'SET_EXERCISE_REQUEST_SENT',
    SET_EXERCISE_RECEIVED: 'SET_EXERCISE_RECEIVED',
    SELECT_INTERVAL: 'SELECT_INTERVAL',
    SELECT_KIND: 'SELECT_KIND',
    SELECT_LENGTH: 'SELECT_LENGTH',
    EXERCISE_RUN: 'EXERCISE_RUN',
    EXERCISE_NEXT: 'EXERCISE_NEXT',
    SUPPLY_ANSWER: 'SUPPLY_ANSWER',
    EXERCISE_COMPLETE: 'EXERCISE_COMPLETE',
};

export const RunnerStage = {
    OFFLINE: 'OFFLINE',
    AT_START: 'AT_START',
    RUNNING: 'RUNNING',
    ANSWER_WAITING: 'ANSWER_WAITING',
    ANSWER_PROVIDED: 'ANSWER_PROVIDED'
};


export const RunnerResult = {  // TODO: merge with enums
    CANCELLED: 0,
    COMPLETED_MISTAKE: -1,
    COMPLETED_CORRECT: 1,
};


export function exerciseRunnerOpen() {
    return {
        type: ExerciseActions.EXERCISE_RUNNER_OPEN,
    }
}

export function kindsLoading() {
    return {
        type: ExerciseActions.KINDS_LOADING,
    }
}

export function kindsReceived(kinds) {
    return {
        type: ExerciseActions.KINDS_RECEIVED,
        kinds
    }
}

export function setExerciseReceived(exercise) {
    return {
        type: ExerciseActions.SET_EXERCISE_RECEIVED,
        exercise
    }
}

export function setExerciseRequested() {
    return {
        type: ExerciseActions.SET_EXERCISE_REQUEST_SENT
    }
}

export function selectKind(kind) {
    return {
        type: ExerciseActions.SELECT_KIND,
        kind
    }
}


export function selectLength(length) {
    return {
        type: ExerciseActions.SELECT_LENGTH,
        length
    }
}


export function exerciseRun() {
    return {
        type: ExerciseActions.EXERCISE_RUN
    }
}


export function supplyAnswer(answer) {
    return {
        type: ExerciseActions.SUPPLY_ANSWER,
        answer: parseInt(answer),
    }
}


export function nextNumber() {
    return {
        type: ExerciseActions.EXERCISE_NEXT
    }
}

export function exerciseComplete() {
    return {
        type: ExerciseActions.EXERCISE_COMPLETE
    }
}


export function changeInterval(intervalValue) {
    return {
        type: ExerciseActions.SELECT_INTERVAL,
        intervalSelected: intervalValue
    }
}

export function exerciseCancel() {
    return {
        type: ExerciseActions.EXERCISE_CANCEL
    }
}