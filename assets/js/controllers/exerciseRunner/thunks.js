import {
    exerciseRun, setExerciseRequested, setExerciseReceived,
    kindsLoading, kindsReceived, exerciseComplete,
    exerciseCancel} from './actions';


export function onGoPressed() {
    return function (dispatch, getState) {
        dispatch(setExerciseRequested());
        const rs = getState().exerciseRunner;
        const params = {
            kind: rs.kindSelected,
            length: rs.lengthSelected,
            csrfmiddlewaretoken: Django.csrf_token()
        };
        const searchParams = new URLSearchParams();
        for (const prop in params) {
          searchParams.set(prop, params[prop]);
        }
        return fetch(
            Django.url('generate-exercise'),
            {
                headers: {'X-Requested-With': 'XMLHttpRequest'},
                method: 'POST',
                body: searchParams,
                credentials: 'same-origin'
            },
        )
        .then(response => response.json())
        .then(exercise => dispatch(setExerciseReceived(exercise)))
            .then(() => dispatch(exerciseRun()))
    }
}

export function loadKinds() {
    return function(dispatch) {
        dispatch(kindsLoading());
        return fetch(
            Django.url('kind-list'),
            {
                mode: 'same-origin',
                credentials: 'same-origin'
            }
        )
        .then(response => response.json())
            .then(kinds => dispatch(kindsReceived(kinds)))
    }
}

function postData(state) {
    const rs = state.exerciseRunner;
    const data = {
        result: rs.runnerResult,
        exercise: rs.exercise,
        interval: rs.intervalSelected,
        skill_level: rs.kindSelected,
        answer: rs.answer,
        opened: rs.opened,
        requested: rs.requested,
        received: rs.received,
        started: rs.started,
        cancelled: rs.cancelled,
        closed: rs.closed,
        answer_requested: rs.answerRequested,
        answer_provided: rs.answerProvided
    };
    return fetch(
        Django.url('exerciseanalytics-list', 'json'),
        {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRFToken': Django.csrf_token()
            },
            method: "POST",
            body: JSON.stringify(data),
            mode: 'same-origin',
            credentials: 'same-origin'
        }
    )
}

export function completeExercise() {
    return function (dispatch, getState) {
        dispatch(exerciseComplete());
        postData(getState());
    }
}

export function cancelExercise() {
    return function (dispatch, getState) {
        dispatch(exerciseCancel());
        postData(getState());

    }
}