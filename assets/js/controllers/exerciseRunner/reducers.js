import { ExerciseActions, RunnerStage, RunnerResult } from './actions';


const exerciseRunnerInitialState = {
    exercise: null,
    stage: RunnerStage.OFFLINE,
    answer: null,
    answerIsCorrect: null,
    currentIndex: 0,
    intervalSelected: null,
    lengthSelected: null,
    kindsRequestPending: false,
    kindSelected: null,
    intervalsAvailable: [500, 1000, 2000],
    lengthsAvailable: [5, 7, 10, 15, 20],
    kindsAvailable: [],
    generateRequestPending: false,
    opened: null,
    requested: null,
    received: null,
    started: null,
    cancelled: null,
    closed: null,
    answerRequested: null,
    answerProvided: null,
    runnerResult: null,
};


function getRunnerResult(exercise, answer) {
    let sum = exercise.contents.reduce((a, b) => a + b, 0);
    if (answer === sum) {
        return RunnerResult.COMPLETED_CORRECT;
    } else {
        return RunnerResult.COMPLETED_MISTAKE;
    }
}


function ExerciseRunner(state = exerciseRunnerInitialState, action) {
    if (action.type === ExerciseActions.EXERCISE_RUNNER_OPEN) {
        if (state.stage !== RunnerStage.OFFLINE) return state;
        return {
            ...exerciseRunnerInitialState,
            stage: RunnerStage.AT_START,
            kindsAvailable: state.kindsAvailable, // TODO: ugly
            opened: new Date()
        };
    }
    if (action.type === ExerciseActions.KINDS_LOADING) {
        return {
            ...state,
            kindsRequestPending: true,
        }
    }
    if (action.type === ExerciseActions.KINDS_RECEIVED) {
        return {
            ...state,
            kindsRequestPending: false,
            kindsAvailable: action.kinds
        }
    }
    if (action.type === ExerciseActions.EXERCISE_CANCEL) {
        if (state.stage !== RunnerStage.AT_START) return state;
        return {
            ...state,
            cancelled: new Date(),
            runnerResult: RunnerResult.CANCELLED,
            stage: RunnerStage.OFFLINE
        }
    }
    if (action.type === ExerciseActions.SET_EXERCISE_REQUEST_SENT) {
        return {
            ...state,
            generateRequestPending: true,
            requested: new Date(),
            exercise: null  // Reset exercise when requested
        }
    }
    if (action.type === ExerciseActions.SET_EXERCISE_RECEIVED) {
        return {
            ...state,
            received: new Date(),
            generateRequestPending: false,
            exercise: action.exercise
        }
    }
    if (action.type === ExerciseActions.SELECT_INTERVAL) {
        return {
            ...state,
            intervalSelected: action.intervalSelected
        }
    }
    if (action.type === ExerciseActions.SELECT_KIND) {
        return {
            ...state,
            kindSelected: action.kind
        }
    }
    if (action.type === ExerciseActions.SELECT_LENGTH) {
        return {
            ...state,
            lengthSelected: action.length
        }
    }
    if (action.type === ExerciseActions.EXERCISE_RUN) {
        if (state.stage !== RunnerStage.AT_START) return state;
        return {
            ...state,
            stage: RunnerStage.RUNNING,
            started: new Date()
        }
    }
    if (action.type === ExerciseActions.EXERCISE_NEXT) {
        const { currentIndex, exercise, stage } = state;
        if (stage !== RunnerStage.RUNNING) return state;
        if (currentIndex < exercise.contents.length - 1) {
            return {...state, currentIndex: currentIndex + 1}
        } else {
            return {
                ...state,
                stage: RunnerStage.ANSWER_WAITING,
                answerRequested: new Date()
            }
        }
    }
    if (action.type === ExerciseActions.SUPPLY_ANSWER) {
        if (state.stage !== RunnerStage.ANSWER_WAITING) return state;
        return {
            ...state,
            stage: RunnerStage.ANSWER_PROVIDED,
            answer: action.answer,
            answerProvided: new Date(),
            runnerResult: getRunnerResult(state.exercise, action.answer)
        }
    }
    if (action.type === ExerciseActions.EXERCISE_COMPLETE) {
        if (state.stage !== RunnerStage.ANSWER_PROVIDED) return state;
        return {
            ...state,
            stage: RunnerStage.OFFLINE,
            closed: new Date()
        }
    }
    return state;
}

export default ExerciseRunner;