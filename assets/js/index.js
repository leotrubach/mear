import React from 'react';
import MearApp from './components/app';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import rootReducer from './controllers/reducers'
import thunkMiddleware from 'redux-thunk'
import fetch from 'isomorphic-fetch';
import * as actions from './controllers/exerciseRunner/actions';


let store = createStore(
    rootReducer,
    applyMiddleware(thunkMiddleware)
);


// TODO: rewrite navbar
window.reactStore = store;
window.reactActions = actions;


ReactDOM.render(
  <Provider store={store}>
    <MearApp />
  </Provider>,
  document.getElementById('container')
);
