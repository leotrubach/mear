import React from 'react';
import ExerciseRunner from './exerciseRunner';
import {Button} from 'reactstrap';


class MearApp extends React.Component {
    constructor(props) {
        super(props);
        this.state = {rowId: 0};
    }

    render() {
        const { store } = this.context;

        return (
            <div className="container">
                <ExerciseRunner/>
            </div>
        )
    }
}

MearApp.contextTypes = { store: React.PropTypes.object };

export default MearApp;