import React from 'react';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter, Input, Label, Tag } from 'reactstrap';
import {
    RunnerStage, RunnerResult, exerciseRun, supplyAnswer,
    nextNumber, changeInterval, selectLength, selectKind
} from '../controllers/exerciseRunner/actions';

import {
    loadKinds, completeExercise, cancelExercise, onGoPressed
} from '../controllers/exerciseRunner/thunks';

import { connect } from 'react-redux';

const hiddenInterval = 100;  // Timeout interval 100msecs

class NumberSwitcher extends React.Component {
    constructor() {
        super();
        this.state = {
            numberVisible: true
        };
    }

    animateNumbers() {
        const { interval } = this.props;
        const duration = this.props.numbers.length * interval;
        const visibleOffset = interval - hiddenInterval;
        const startTime = window.performance.now();
        let lastNumberChange = startTime;
        let lastVisible = false;

        function step() {
            const currtime = window.performance.now();

            if (currtime - startTime > duration) {
                this.props.dispatch(nextNumber());
                return;
            }

            const intervalOffset = currtime - lastNumberChange;
            const currVisible = intervalOffset < visibleOffset;

            if (currVisible !== lastVisible) {
                lastVisible = currVisible;
                this.setState({
                    numberVisible: currVisible
                });
            }
            if (intervalOffset > interval) {
                lastNumberChange += interval;
                this.props.dispatch(nextNumber());
            }
            requestAnimationFrame(step);
        }
        step = step.bind(this);
        requestAnimationFrame(step);
    }

    componentDidMount() {
        this.animateNumbers();
    }

    componentWillUnmount() {

    }

    render() {
        let classes = 'counter-number text-center';
        if (!this.state.numberVisible) {
            classes = classes + ' hidden-xs-up';
        }
        return (
            <h1 className={classes}>
                {this.props.numbers[this.props.currentIndex]}
            </h1>);
    }
}


class ExerciseRunner extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.completeExercise = this.completeExercise.bind(this);
        this.runExercise = this.runExercise.bind(this);
        this.cancelExercise = this.cancelExercise.bind(this);
        this.handleAnswerInput = this.handleAnswerInput.bind(this);
        this.selectKind = this.selectKind.bind(this);
        this.selectLength = this.selectLength.bind(this);
    }

    toggle() {

    }

    componentDidMount() {
        this.props.dispatch(loadKinds());
    }

    changeInterval(intervalValue) {
        this.props.dispatch(changeInterval(intervalValue))
    }

    completeExercise() {
        this.props.dispatch(completeExercise());
    }

    selectKind(e) {
        let kind = parseInt(e.target.value);
        this.props.dispatch(selectKind(kind));
    }

    selectLength(e) {
        let length = e.target.value;
        this.props.dispatch(selectLength(length));
    }

    runExercise() {
        this.props.dispatch(onGoPressed());
    }

    cancelExercise() {
        this.props.dispatch(cancelExercise());
    }

    handleAnswerInput(event) {
        if (event.key === 'Enter') {
            event.preventDefault();
            let val = parseInt(event.target.value, 10);
            if (!isNaN(val)) this.props.dispatch(supplyAnswer(val));
        }
        if (isNaN(parseInt(event.key, 10))) {
            event.preventDefault();
        }
    }

    renderBody() {
        if (this.props.rs.stage === RunnerStage.AT_START) {
            return (
                <div className="container">
                <div className="container category-selector">
                    <h1>{gettext('Select category')}</h1>
                    <Input
                        type="select"
                        size="lg"
                        value={this.props.rs.kindSelected?this.props.rs.kindSelected:""}
                        onChange={this.selectKind}>
                            <option key="category-none">------</option>

                            {this.props.rs.kindsAvailable.map((v) =>
                                <option
                                    key={`category-${v.value}`}
                                    value={v.value}>{v.name}</option>)}
                    </Input>
                </div>
                <div className="container category-selector">
                    <h1>{gettext('Select length')}</h1>
                    <Input
                        type="select"
                        size="lg"
                        value={this.props.rs.lengthSelected?this.props.rs.lengthSelected:""}
                        onChange={this.selectLength}>
                            <option key="category-none">------</option>

                            {this.props.rs.lengthsAvailable.map((v) =>
                                <option
                                    key={`length-${v}`}
                                    value={v}>{v}</option>)}
                    </Input>
                </div>
                <div className="container interval-selector">
                    <h1 className="interval-selector-label">{gettext('Select interval')}</h1>
                    {this.props.rs.intervalsAvailable.map((v) =>
                        <Button
                            className="interval-selector-button"
                            size="lg"
                            key={`interval-${v}`}
                            color={this.props.rs.intervalSelected===v?"success":"primary"}
                            onClick={()=>this.changeInterval(v)}>{v / 1000}</Button>
                    )}
                </div>
                </div>
            )
        }
        if (this.props.rs.stage === RunnerStage.RUNNING) {
            return <NumberSwitcher
                numbers={this.props.rs.exercise.contents}
                currentIndex={this.props.rs.currentIndex}
                interval={this.props.rs.intervalSelected}
                dispatch={this.props.dispatch} />
        }
        if (this.props.rs.stage === RunnerStage.ANSWER_WAITING) {
            return (
                <div className="container">
                    <Label className="answer-input-label" for="answer">{gettext('Enter your answer:')}</Label>
                    <Input className="answer-input" id="answer" onKeyPress={this.handleAnswerInput} autoFocus />
                </div>);
        }
        if (this.props.rs.stage === RunnerStage.ANSWER_PROVIDED) {
            let tagTxt;
            if (this.props.rs.runnerResult === RunnerResult.COMPLETED_CORRECT) {
                tagTxt = gettext('Your answer is %s. It is <span class="tag tag-success">CORRECT</span>');
            } else {
                tagTxt = gettext('Your answer is %s. It is <span class="tag tag-danger">NOT CORRECT</span>');
            }
            const resultTxt = interpolate(tagTxt, [this.props.rs.answer]);
            return (
                <p dangerouslySetInnerHTML={{__html: resultTxt}} />
            )
        }
    }

    renderFooter() {
        if (this.props.rs.stage === RunnerStage.AT_START) {
            return (
                <div className="container">
                    <Button onClick={this.cancelExercise}>{gettext('Cancel')}</Button>
                    <Button
                        color="primary"
                        onClick={this.runExercise}
                        disabled={!this.props.rs.kindSelected || !this.props.rs.lengthSelected || !this.props.rs.intervalSelected}>{gettext('Go!')}</Button>
                </div>
            )
        }
        if (this.props.rs.stage === RunnerStage.ANSWER_PROVIDED) {
            return <Button color="primary" onClick={this.completeExercise} autoFocus>{gettext('Finish')}</Button>
        }
    }

    renderHeader() {
        if (this.props.modal) {
            if (this.props.rs.kindSelected) {
                return this.props.rs.kindSelected.name;
            } else {
                return "";
            }
        }
    }

    render() {
        return (
            <div>
                <Modal isOpen={this.props.modal} toggle={this.toggle} backdrop="static" className="exercise-modal">
                    <ModalHeader>{this.renderHeader()}</ModalHeader>
                    <ModalBody>
                        <div className="modal-body-container">
                            <div className="container">
                                {this.renderBody()}
                            </div>
                        </div>
                    </ModalBody>
                    <ModalFooter>
                        {this.renderFooter()}
                    </ModalFooter>
                </Modal>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    const s = state.exerciseRunner;
    return {
        modal: s.stage !== RunnerStage.OFFLINE,
        rs: s
    }
};

export default connect(mapStateToProps)(ExerciseRunner);