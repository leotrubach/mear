import argparse
import os
from os.path import join as j, abspath as ap, dirname as dn
from subprocess import call
import tempfile
import random
import string

PASS_LETTERS = string.ascii_letters + string.digits


def randstr(length=10, symbols=PASS_LETTERS):
    return ''.join([random.choice(symbols) for _ in range(length)])


PASSWORD = randstr()


BASE_DIR = dn(ap(__file__))
ROOT_DIR = dn(BASE_DIR)
VIRTUALENV_PATH = os.environ['VIRTUAL_ENV']

STATIC_DIR = j(ROOT_DIR, 'static')
MEDIA_DIR = j(ROOT_DIR, 'media')
NODE_MODULES_DIR = j(BASE_DIR, 'node_modules')

PROJECT_DIR = j(BASE_DIR, 'mear')
STATIC_SETTINGS_FILE = j(PROJECT_DIR, 'static_settings.py')

SUPERVISOR_CONFD = '/etc/supervisor/conf.d/'
SUPERVISOR_CONF_FILE = j(SUPERVISOR_CONFD, 'mear.conf')

MANAGE_PY_PATH = j(BASE_DIR, 'manage.py')

SUPERVISOR_CONF_CONTENTS = """
[program:daphne]
command={virtualenv_path}/bin/daphne -b 127.0.0.1 -p 8001 mear.asgi:channel_layer
user=user
directory={project_path}

[program:daphne_worker]
command={virtualenv_path}/bin/python manage.py runworker
user=user
directory={project_path}
numprocs=4
process_name=%(program_name)s[%(process_num)s]
"""

NGINX_CONFIG_DIR = '/etc/nginx'
SITES_AVAILABLE = j(NGINX_CONFIG_DIR, 'sites-available')
SITES_ENABLED = j(NGINX_CONFIG_DIR, 'sites-enabled')
NGINX_CONF_FILE = j(SITES_AVAILABLE, 'mear.conf')
NGINX_LINK_FILE = j(SITES_ENABLED, 'mear.conf')

NGINX_CONFIG_CONTENTS = """
upstream daphne {{
    server 127.0.0.1:8001;
}}

server {{
    listen      80;
    server_name {server_name};
    charset     utf-8;

    client_max_body_size 75M;

    location /media  {{
        alias {media_path};
    }}

    location /static {{
        alias {static_path};
    }}

    location /node_modules {{
        alias {node_modules_path};
    }}

    location / {{
        proxy_pass http://daphne;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_set_header Host            $host;
        proxy_set_header X-Forwarded-For $remote_addr;
    }}
}}
"""

STATIC_SETTINGS_CONTENTS = """
STATIC_ROOT = '{static_path}'
"""

def param_dict(args):
    return {
        'project_path': BASE_DIR,
        'user_name': args.user,
        'virtualenv_path': VIRTUALENV_PATH,
        'media_path': MEDIA_DIR,
        'static_path': STATIC_DIR,
        'node_modules_path': NODE_MODULES_DIR,
        'server_name': args.server_name,
        'password': PASSWORD,
        'manage_py_path': MANAGE_PY_PATH,
    }


def gen_nginx_conf_contents(args):
    return NGINX_CONFIG_CONTENTS.format(**param_dict(args))


def nginx_install(args):
    with tempfile.NamedTemporaryFile(delete=False, mode='w') as f:
        f.write(gen_nginx_conf_contents(args))
    install_file(f.name, NGINX_CONF_FILE)
    os.unlink(f.name)
    call(['ln', '-sf', NGINX_CONF_FILE, NGINX_LINK_FILE])


def static_install(args):
    with tempfile.NamedTemporaryFile(delete=False, mode='w') as f:
        f.write(STATIC_SETTINGS_CONTENTS.format(**param_dict(args)))
    call(['cp', f.name, STATIC_SETTINGS_FILE])
    call(['chown', args.user, STATIC_SETTINGS_FILE])
    call(['chmod', '664', STATIC_SETTINGS_FILE])
    call(['python', 'manage.py', 'collectstatic', '--noinput'])


def supervisor_install(args):
    with tempfile.NamedTemporaryFile(delete=False, mode='w') as f:
        f.write(gen_supervisor_conf_contents(args))
    install_file(f.name, SUPERVISOR_CONF_FILE)
    os.unlink(f.name)
    call(['supervisorctl', 'reread'])
    call(['supervisorctl', 'update'])


def install_file(src_file, dest_file):
    call(['install', src_file, dest_file])


def create_dirs(args):
    """
    Create dirs and set permissions
    :param args: command line arguments values
    """
    for d in (STATIC_DIR, MEDIA_DIR, NODE_MODULES_DIR):
        call(['mkdir', '-p', d])
        call(['chown', 'user:www-data', d])
        call(['chmod', '-R', '755', d])
        call(['chmod', '-R', 'g+s', d])
    call(['chmod', '-R', '775', MEDIA_DIR])


def service_reload():
    call(['service', 'nginx', 'restart'])


def parse_args():
    a = argparse.ArgumentParser()
    a.add_argument('user', help='User for uwsgi process')
    a.add_argument('--server-name', default='menar.1ge.kz',
                   help='Server name')
    a.add_argument('--no-static', help='No static maintenance', action='store_true')
    return a.parse_args()


def gen_supervisor_conf_contents(args):
    return SUPERVISOR_CONF_CONTENTS.format(**param_dict(args))


def add_user_to_www(args):
    call(['adduser', args.user, 'www-data'])


def install(args):
    create_dirs(args)
    if not args.no_static:
        static_install(args)
    nginx_install(args)
    supervisor_install(args)
    add_user_to_www(args)
    service_reload()


def main():
    args = parse_args()
    install(args)


if __name__ == '__main__':
    main()