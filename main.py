import sys
import os

sys.path.append(os.path.dirname(__file__))

from abacus.exgen import FamilyPlusGenerator

def main():
    g = FamilyPlusGenerator(10, 10)
    #for s in g:
    #    print(' '.join(map(str, s)), U.sequence_stats(s))

    c, good = 0, 0
    states = g.fgenerate()
    for s in states:
        print(' '.join(map(str, s)), sum(s), U.sequence_stats(s))


if __name__ == '__main__':
    main()