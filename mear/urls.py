from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView
from django.views.i18n import JavaScriptCatalog
from django.contrib.auth import views as auth_views
from edu import urls as edu_urls


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^edu/', include(edu_urls)),
    url(r'^accounts/login/$', auth_views.login, name='login'),
    url(r'^accounts/logout/$', auth_views.logout, name='logout'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^$', TemplateView.as_view(template_name="index.html"), name='index'),
    url(r'^jsi18n/$', JavaScriptCatalog.as_view(), name='javascript-catalog'),
    url(r'^djangojs/', include('djangojs.urls')),
]

if settings.DEBUG:
    urlpatterns += static('/node_modules', document_root=settings.NODE_MODULES_PATH)