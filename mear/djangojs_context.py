from djangojs.context_serializer import ContextSerializer
from edu.rest.serializers import ProfileSerializer
from edu import models as M
from edu import enums as E


class OWLContextSerializer(ContextSerializer):
    def handle_user(self, data):
        super().handle_user(data)
        user = self.request.user
        if user.is_authenticated():
            try:
                data['user']['profile'] = ProfileSerializer(user.profile).data
            except M.UserProfile.DoesNotExist:
                pass
        data['enums'] = {'skillLevelChoices': {k: str(v) for k, v in E.SKILL_LEVEL_CHOICES}}