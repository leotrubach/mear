import itertools
import functools
import operator
from collections import Counter, defaultdict


from . import utils as U
from . import atypes as T

NEGATIVES = set(range(-9, 0))
POSITIVES = set(range(1, 10))
SINGLE_DIGIT = NEGATIVES | POSITIVES




class SeqGenerator(object):
    allowed_kinds = tuple()

    def optgen(self):
        raise NotImplementedError

    def __init__(self, length, count):
        self.random = U.Random36()
        self.rel = T.DigitRelations()
        self.length = length
        self.count = count
        self._reset()

    def _reset(self):
        self.cur = 0
        self.state = []
        self.candidates = []
        self.operation_stats = []
        self.preferred_kind_found = False

    def do_generation(self):
        for _ in range(self.length):
            self.optgen()
            picked = self.choose()
            self.cur += picked
            self.state.append(picked)

    def weigh(self, x, s):
        if self.state:
            if x == -self.state[-1]:
                return 0
        return 100

    def result_set(self):
        return range(10)

    def choose(self):
        weights = [self.weigh(x, s) for x, s in zip(self.candidates, self.operation_stats)]
        return self.random.choices(self.candidates, weights=weights)[0]

    def __iter__(self):
        return self

    def __next__(self):
        self._reset()
        self.do_generation()
        return tuple(self.state)


class SeqGeneratorSingleDigit(SeqGenerator):
    def optgen(self):
        options = set()
        for ak in self.allowed_kinds:
            options.update(self.rel.fosl[(self.cur, *ak)])
        options.discard(0)
        candidates = list(options)
        possible_results = [v + self.cur for v in candidates]
        result_set = self.result_set()
        self.candidates = [v for v, r in zip(candidates, possible_results) if r in result_set]
        self.operation_stats = [1] * len(self.candidates)


class DoubleGenerator(SeqGenerator):
    allowed_kinds = tuple()
    no_single_digits = False
    single_digit_only = False
    avoid_elevens_in_state = False
    preferred_kind = None
    unique_elements_in_state = False
    unique_window = 0
    plus_only = False
    search_depth = 2

    def __init__(self, length, count):
        super().__init__(length, count)
        self.allowed_set = set(self.allowed_kinds)

    def optgen(self):
        options = [x - self.cur for x in range(100) if x != self.cur]
        if self.single_digit_only:
            options = [o for o in options if abs(o) < 10]
        if self.no_single_digits:
            options = [o for o in options if abs(o) > 10]
        self.candidates = options
        self.operation_stats = [U.operation_stats(self.cur, o)[1] for o in self.candidates]
        stat_sets = [s.get_set() for s in self.operation_stats]
        self.candidates, self.operation_stats = tuple(zip(*[
            (o, s) for o, s, ss in zip(self.candidates, self.operation_stats, stat_sets)
            if ss.issubset(self.allowed_set)]))

    def foptgen(self, state):
        cur = sum(state)
        options = tuple(x - cur for x in range(100) if x != self.cur)
        if state:
            options = tuple(x for x in options if -x != state[-1])
        if self.plus_only:
            options = tuple(o for o in options if o > 0)
        if self.unique_elements_in_state:
            options = tuple(x for x in options if x not in state)
        if self.single_digit_only:
            options = tuple(o for o in options if abs(o) < 10)
        if self.no_single_digits:
            options = tuple(o for o in options if abs(o) > 10)
        if self.avoid_elevens_in_state:
            opts = tuple(o for o in options if not (o % 11 == 0 or (o + cur) % 11 == 0))
            if opts:
                options = opts
        if self.unique_window:
            options = tuple(o for o in options if o not in state[-self.unique_window:])
        operation_stats = tuple(U.operation_stats(cur, o)[1] for o in options)
        for o, s in zip(options, operation_stats):
            if s.get_set().issubset(self.allowed_set):
                yield o, s

    def search(self, state: tuple=(), stats=T.OperationStatsOp.empty(), cur_depth=0, max_depth=None):
        if max_depth is None:
            max_depth = self.search_depth
        r = defaultdict(list)
        if cur_depth == max_depth - 1:
            for o, s in self.foptgen(state):
                new_state = (*state, o)
                new_stats = T.OperationStatsOp(*[(a + b) for a, b in zip(stats, s)])
                pc = new_stats.get_ok(self.preferred_kind)
                r[pc].append(new_state)
            for v in r.values():
                self.random.shuffle(v)
            for k in sorted(r.keys(), reverse=True):
                for el in r[k]:
                    yield k, el
        else:
            for o, s in self.foptgen(state):
                new_state = (*state, o)
                new_stats = T.OperationStatsOp(*[(a + b) for a, b in zip(stats, s)])

                for pc, st in self.search(new_state, new_stats, cur_depth + 1, max_depth):
                    r[pc].append(st)
            for v in r.values():
                self.random.shuffle(v)
            for k in sorted(r.keys(), reverse=True):
                for el in r[k]:
                    yield k, el

    def fgenerate(self):
        to_gen = self.length
        states = tuple(tuple() for _ in range(self.count))
        while to_gen > 0:
            if to_gen < self.search_depth:
                depth = to_gen
            else:
                depth = self.search_depth
            new_states = []
            for state in states:
                try:
                    _, s = next(self.search(state, max_depth=depth))
                except StopIteration:
                    pass
                else:
                    new_states.append(s)
            states = tuple(new_states)
            to_gen -= self.search_depth
        return states

    def weigh(self, x, s):
        if self.state:
            if x == -self.state[-1]:
                return 0
            if x % 10 == 0:
                return 10
            if self.preferred_kind:
                total_preferred = s.get(self.preferred_kind, T.Operation.plus) + s.get(self.preferred_kind, T.Operation.minus)
                if total_preferred == 0:
                    return 10
                if total_preferred == 1:
                    return 50
        return 100


class NoFormulaGenerator(DoubleGenerator):
    preferred_kind = T.OperationKind.noformula
    unique_window = 1
    single_digit_only = True
    allowed_kinds = (
        (T.OperationKind.noformula, T.Operation.minus),
        (T.OperationKind.noformula, T.Operation.plus),
    )


class NoFormulaSameGenerator(NoFormulaGenerator):
    def __next__(self):
        return tuple(x * 11 for x in super().__next__())

    def fgenerate(self):
        states = super().fgenerate()
        return tuple(
            tuple(e * 11 for e in s) for s in states
        )


class NoFormulaDoubleGenerator(DoubleGenerator):
    preferred_kind = T.OperationKind.noformula
    allowed_kinds = (
        (T.OperationKind.noformula, T.Operation.minus),
        (T.OperationKind.noformula, T.Operation.plus)
    )
    no_single_digits = True


class PartnerGeneratorSingleDigit(DoubleGenerator):
    single_digit_only = True
    preferred_kind = T.OperationKind.partner
    allowed_kinds = (
        (T.OperationKind.noformula, T.Operation.minus),
        (T.OperationKind.noformula, T.Operation.plus),
        (T.OperationKind.partner, T.Operation.minus),
        (T.OperationKind.partner, T.Operation.plus),
    )


class PartnerGeneratorDoubleDigitSame(PartnerGeneratorSingleDigit):
    def __next__(self):
        return tuple(x * 11 for x in super().__next__())

    def fgenerate(self):
        states = super().fgenerate()
        return tuple(
            tuple(e * 11 for e in s) for s in states
        )


class PartnerGeneratorDoubleDigitDifferent(PartnerGeneratorSingleDigit):
    single_digit_only = False
    no_single_digits = True
    avoid_elevens = True
    unique_elements_in_state = True


class FriendsGenerator(DoubleGenerator):
    preferred_kind = T.OperationKind.friend
    allowed_kinds = (
        (T.OperationKind.noformula, T.Operation.minus),
        (T.OperationKind.noformula, T.Operation.plus),
        (T.OperationKind.partner, T.Operation.minus),
        (T.OperationKind.partner, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.minus)
    )


class FriendGeneratorSingleDigit(FriendsGenerator):
    single_digit_only = True
    unique_window = 3


class FriendsGeneratorSingleDigitPlusOnly(FriendGeneratorSingleDigit):
    plus_only = True
    allowed_kinds = (
        (T.OperationKind.noformula, T.Operation.minus),
        (T.OperationKind.noformula, T.Operation.plus),
        (T.OperationKind.partner, T.Operation.minus),
        (T.OperationKind.partner, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.plus)
    )


class BigFamilySingleDigitGenerator(DoubleGenerator):
    single_digit_only = True
    preferred_kind = T.OperationKind.family
    allowed_kinds = (
        (T.OperationKind.noformula, T.Operation.minus),
        (T.OperationKind.noformula, T.Operation.plus),
        (T.OperationKind.partner, T.Operation.minus),
        (T.OperationKind.partner, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.minus),
        (T.OperationKind.family, T.Operation.plus),
    )


class FamilySingleDigitGenerator(DoubleGenerator):
    single_digit_only = True
    preferred_kind = T.OperationKind.family
    unique_window = 3
    allowed_kinds = (
        (T.OperationKind.noformula, T.Operation.minus),
        (T.OperationKind.noformula, T.Operation.plus),
        (T.OperationKind.partner, T.Operation.minus),
        (T.OperationKind.partner, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.minus),
        (T.OperationKind.family, T.Operation.plus),
        (T.OperationKind.family, T.Operation.minus),
    )


class FriendsPlusGenerator(FriendsGenerator):
    no_single_digits = True
    unique_window = 2
    allowed_kinds = (
        (T.OperationKind.noformula, T.Operation.minus),
        (T.OperationKind.noformula, T.Operation.plus),
        (T.OperationKind.partner, T.Operation.minus),
        (T.OperationKind.partner, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.plus),
    )


class FriendsAllGenerator(FriendsGenerator):
    no_single_digits = True
    unique_window = 2


class FamilyPlusGenerator(DoubleGenerator):
    no_single_digits = True
    preferred_kind = T.OperationKind.family
    allowed_kinds = (
        (T.OperationKind.noformula, T.Operation.minus),
        (T.OperationKind.noformula, T.Operation.plus),
        (T.OperationKind.partner, T.Operation.minus),
        (T.OperationKind.partner, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.minus),
        (T.OperationKind.family, T.Operation.plus),
    )


class FamilyGenerator(DoubleGenerator):
    no_single_digits = True
    preferred_kind = T.OperationKind.family
    allowed_kinds = (
        (T.OperationKind.noformula, T.Operation.minus),
        (T.OperationKind.noformula, T.Operation.plus),
        (T.OperationKind.partner, T.Operation.minus),
        (T.OperationKind.partner, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.plus),
        (T.OperationKind.friend, T.Operation.minus),
        (T.OperationKind.family, T.Operation.plus),
        (T.OperationKind.family, T.Operation.minus),
    )

