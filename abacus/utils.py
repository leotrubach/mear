import array
import itertools
import random
from bisect import bisect
from . import atypes as T


def not_diff_sign(x, y):
    return x == 0 or y == 0 or (x > 0) == (y > 0)


def sign(v):
    if v == 0:
        return 0
    return v / abs(v)


def digits(num):
    n = abs(num)
    s = sign(num)
    result = []
    cur = n
    while cur > 0:
        result.append(s * (cur % 10))
        cur //= 10
    return result[::-1]


def extend_by_zeros(n):
    def f(d):
        return [0] * (n - len(d)) + d

    return f


class Random36(random.Random):
    "Show the code included in the Python 3.6 version of the Random class"

    def choices(self, population, weight_func=None, weights=None, *, cum_weights=None, k=1):
        """Return a k sized list of population elements chosen with replacement.

        If the relative weights or cumulative weights are not specified,
        the selections are made with equal probability.

        """
        random = self.random
        if cum_weights is None:
            if weights is None:
                if weight_func is None:
                    _int = int
                    total = len(population)
                    return [population[_int(random() * total)] for i in range(k)]
                weights = [weight_func(o) for o in population]
            cum_weights = list(itertools.accumulate(weights))
        elif weights is not None:
            raise TypeError('Cannot specify both weights and cumulative weights')
        if len(cum_weights) != len(population):
            raise ValueError('The number of weights does not match the population')
        total = cum_weights[-1]
        return [population[bisect(cum_weights, random() * total)] for i in range(k)]


def operation_stats_arr(f, l):
    dstats = array.array('l', [0] * 8)
    stats = array.array('l', [0] * 4)
    rel = T.DigitRelations()
    if l < 0:
        assert f > l
        ok = T.Operation.minus
    else:
        ok = T.Operation.plus
    df, dl = map(digits, (f, l))
    ldf, ldl = map(extend_by_zeros(max(len(df), len(dl)) + 1), (df, dl))
    rl = ldf[:]
    for i in range(len(ldf)):
        if ldl[i] == 0:
            continue  # Skip if simply zeros
        ot = rel.flo[ldf[i], ldl[i]]
        dstats[ot * 2 + ok] += 1
        stats[ot] += 1
        digit_sum = ldf[i] + ldl[i]
        if digit_sum > 9:
            for j in range(i-1, 0, -1):
                ot = rel.flo[rl[j], 1]
                dstats[ot * 2 + ok] += 1
                stats[ot] += 1
                if rl[j] < 0:
                    rl[j] += 1
                    break
                else:
                    rl[j] = 0
        elif digit_sum < 0:
            for j in range(i-1, 0, -1):
                ot = rel.flo[rl[j], -1]
                dstats[ot * 2 + ok] += 1
                stats[ot] += 1
                if rl[j] > 0:
                    rl[j] -= 1
                    break
                else:
                    rl[j] = 9
        else:
            rl[i] = digit_sum
    return stats, dstats


def operation_stats(f, l):
    stats, dstats = operation_stats_arr(f, l)
    return T.OperationStats(*stats), T.OperationStatsOp(*dstats)


def sequence_stats(seq):
    acc = seq[0]
    stats, dstats = array.array('l', [0] * 4), array.array('l', [0] * 8)
    for i in range(1, len(seq)):
        s, d = operation_stats_arr(acc, seq[i])
        for j in range(8):
            dstats[j] += d[j]
        for j in range(4):
            stats[j] += s[j]
        acc += seq[i]
    return T.OperationStats(*stats), T.OperationStatsOp(*dstats)