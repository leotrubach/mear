import enum
import itertools
from collections import namedtuple
from typing import NamedTuple, List, Dict, Tuple, Set


class Operation(enum.IntEnum):
    plus = 0
    minus = 1


class OperationKind(enum.IntEnum):
    noformula = 0
    partner = 1
    friend = 2
    family = 3


AbacusDigit= namedtuple('AbacusDigit', ('lower', 'upper'))
AbacusOperation = namedtuple('AbacusOperation', ('lowerdelta', 'upperdelta', 'older'))

OperationStatsOpKeys = [(o, op) for o, op in itertools.product(OperationKind, Operation)]

OperationStats = namedtuple('OperationStats', (o.name for o in OperationKind))
OperationStatsOp = namedtuple(
    'OperationStatsOp', ('{}_{}'.format(o.name, op.name) for o, op in OperationStatsOpKeys))


def get(self, o: OperationKind, op: Operation):
    return self[o * 2 + op]


def get_set(self):
    return set(k for k, v in zip(OperationStatsOpKeys, self) if v)

def empty(cls):
    return cls(*([0] * 8))

def get_ok(self, ok: OperationKind):
    return sum(self[ok * 2:ok * 2 + 2])

OperationStatsOp.get = get
OperationStatsOp.get_set = get_set
OperationStatsOp.empty = classmethod(empty)
OperationStatsOp.get_ok = get_ok



def from_digit(cls: AbacusDigit, digit: int) -> AbacusDigit:
    assert 0 <= digit <= 9
    return cls(digit % 5, int(digit >= 5))

AbacusDigit.from_digit = classmethod(from_digit)


def to_digit(self: AbacusDigit):
    return self.upper * 5 + self.lower

AbacusDigit.to_digit = to_digit


def add(self: AbacusDigit, other: AbacusDigit) -> AbacusOperation:
    s = self.to_digit() + other.to_digit()
    l, u = AbacusDigit.from_digit(s % 10)
    return AbacusOperation(l - self.lower, u - self.upper, int(s >= 10))

AbacusDigit.__add__ = add


def sub(self: AbacusDigit, other: AbacusDigit) -> AbacusOperation:
    underflow = 0
    s = self.to_digit() - other.to_digit()
    if s < 0:
        s += 10
        underflow = -1
    l, u = AbacusDigit.from_digit(s % 10)
    return AbacusOperation(l - self.lower, u - self.upper, underflow)

AbacusDigit.__sub__ = sub


def kind(self: AbacusOperation) -> OperationKind:
    deltas_not_diff_sign = (
        self.lowerdelta == 0 or
        self.upperdelta == 0 or
        (self.lowerdelta > 0) == (self.upperdelta > 0))
    if self.older:
        if deltas_not_diff_sign:
            return OperationKind.friend
        else:
            return OperationKind.family
    else:
        if deltas_not_diff_sign:
            return OperationKind.noformula
        else:
            return OperationKind.partner


AbacusOperation.kind = kind


def num_to_digits(num: int) -> List[AbacusDigit]:
    current_num = num
    result = []
    while current_num > 0:
        rem = current_num % 10
        result.append(AbacusDigit.from_digit(rem))
        current_num //= 10
    return result


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class DigitRelations(object, metaclass=Singleton):
    def __init__(self):
        self.fol = {
            (i, o): set() for i, o in itertools.product(range(10), OperationKind)
        }
        self.flo = dict()
        self.fosl = {
            (i, ok, o): set() for i, ok, o in itertools.product(range(10), OperationKind, Operation)
        }
        for i, j in itertools.product(range(10), range(10)):
            x, y = map(AbacusDigit.from_digit, (i, j))
            p, m = x + y, x - y
            pk, mk = p.kind(), m.kind()
            self.fol[i, pk].add(j)
            self.fol[i, mk].add(-j)
            self.fosl[i, pk, Operation.minus].add(j)
            self.fosl[i, mk, Operation.plus].add(-j)
            self.flo[i, j] = pk
            self.flo[i, -j] = mk

digit_relations = DigitRelations()

