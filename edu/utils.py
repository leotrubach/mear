from abacus import exgen as G
from . import enums as E


skill_gen_mapping = {
    E.SkillLevel.one_digit_no_formula: G.NoFormulaGenerator,
    E.SkillLevel.two_digit_no_formula_same: G.NoFormulaSameGenerator,
    E.SkillLevel.two_digit_no_formula_different: G.NoFormulaDoubleGenerator,
    E.SkillLevel.one_digit_partners: G.PartnerGeneratorSingleDigit,
    E.SkillLevel.two_digit_partners_same: G.PartnerGeneratorDoubleDigitSame,
    E.SkillLevel.two_digit_partners_different: G.PartnerGeneratorDoubleDigitDifferent,
    E.SkillLevel.one_digit_friends_plus: G.FriendsGeneratorSingleDigitPlusOnly,
    E.SkillLevel.one_digit_friends_minus: G.FriendGeneratorSingleDigit,
    E.SkillLevel.one_digit_bigfamily: G.BigFamilySingleDigitGenerator,
    E.SkillLevel.one_digit_smallfamily: G.FamilySingleDigitGenerator,
    E.SkillLevel.two_digit_friends_plus: G.FriendsPlusGenerator,
    E.SkillLevel.two_digit_friends_minus: G.FriendsGenerator,
    E.SkillLevel.two_digit_bigfamily: G.FamilyPlusGenerator,
    E.SkillLevel.two_digit_smallfamily: G.FamilyGenerator
}