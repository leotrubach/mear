from django.shortcuts import render
from django.http import JsonResponse
from django.views.generic import FormView, View
from . import forms as F
from . import utils as U
from . import enums as E


class FormGetView(FormView):

    def get(self, request, *args, **kwargs):
        form_class = self.get_form_class()
        form = form_class(request.GET)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)


class GenExerciseView(FormView):
    form_class = F.GenExerciseForm
    template_name = 'gen_test.html'

    def form_valid(self, form):
        cd = form.cleaned_data
        kind = int(cd['kind'])
        gen = U.skill_gen_mapping[E.SkillLevel(kind)](cd['length'], 1)
        contents = gen.fgenerate()[0]

        if self.request.is_ajax():
            return JsonResponse({'contents': contents})
        else:
            return render(self.request, 'gen_test.html', {
                'contents': ' '.join(map(str, contents)),
                'form': F.GenExerciseForm()
            })


class KindList(View):
    def get(self, request, *args, **kwargs):
        if request.user.profile and request.user.profile.kind == E.UserKind.student.value:
            data = [
                {'value': v, 'name': str(d)}
                for v, d in E.SKILL_LEVEL_CHOICES[:14]
                if v <= request.user.profile.skill_level
            ]
        else:
            data = [
                {'value': v, 'name': str(d)}
                for v, d in E.SKILL_LEVEL_CHOICES[:14]
            ]
        return JsonResponse(data, safe=False)