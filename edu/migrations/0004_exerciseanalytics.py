# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-10 21:38
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion
import edu.enums


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('edu', '0003_auto_20170104_0307'),
    ]

    operations = [
        migrations.CreateModel(
            name='ExerciseAnalytics',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('result', models.IntegerField(blank=True, choices=[(0, 'Cancelled'), (edu.enums.ExerciseResult(-1), 'Completed with mistake'), (edu.enums.ExerciseResult(1), 'Completed correctly')], null=True, verbose_name='Runner result')),
                ('exercise', django.contrib.postgres.fields.jsonb.JSONField(blank=True, null=True, verbose_name='Exercise')),
                ('interval', models.IntegerField(blank=True, null=True, verbose_name='Interval')),
                ('skill_level', models.IntegerField(choices=[(1, 'One digit no formula'), (2, 'Two digit same no formula'), (3, 'Two digit different no formula'), (4, 'One digit partners'), (5, 'Two digit partners same'), (6, 'Two digit partners different'), (7, 'One digit friends plus only'), (8, 'One digit friends minus only'), (9, 'One digit big family'), (10, 'One digit small family'), (11, 'Two digit friends plus'), (12, 'Two digits friends minus'), (13, 'Two digit big family'), (14, 'Two digit small family'), (15, 'Three digit no formula'), (16, 'Three digit formula'), (17, 'Multiplication two digit by one digit'), (18, 'Multiplication three digit by one digit'), (19, 'Multiplication four digit by one digit')], verbose_name='Kind')),
                ('answer', models.TextField(blank=True, null=True, verbose_name='Answer')),
                ('opened', models.DateTimeField(blank=True, null=True, verbose_name='Test form opened')),
                ('requested', models.DateTimeField(blank=True, null=True, verbose_name='Test requested')),
                ('received', models.DateTimeField(blank=True, null=True, verbose_name='Test data received')),
                ('started', models.DateTimeField(blank=True, null=True, verbose_name='Test started')),
                ('cancelled', models.DateTimeField(blank=True, null=True, verbose_name='Test cancelled')),
                ('closed', models.DateTimeField(blank=True, null=True, verbose_name='Test closed')),
                ('answer_requested', models.DateTimeField(blank=True, null=True, verbose_name='Answer requested')),
                ('answer_provided', models.DateTimeField(blank=True, null=True, verbose_name='Answer provided')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Examined user')),
            ],
        ),
    ]
