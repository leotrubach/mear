# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-03 03:42
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.postgres.fields.jsonb
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Exam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('creator', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='Creator')),
            ],
            options={
                'verbose_name': 'Exam',
                'verbose_name_plural': 'Exams',
            },
        ),
        migrations.CreateModel(
            name='ExamExercise',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('rank', models.IntegerField(verbose_name='Rank')),
                ('exam', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='edu.Exam', verbose_name='Exam')),
            ],
            options={
                'verbose_name': 'Exam exercise',
                'verbose_name_plural': 'Exam exercises',
            },
        ),
        migrations.CreateModel(
            name='Exercise',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Created')),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('contents', django.contrib.postgres.fields.jsonb.JSONField(help_text='Contents')),
                ('skill_level', models.IntegerField(verbose_name='Skill level')),
                ('difficulty', models.FloatField(blank=True, null=True, verbose_name='Difficulty')),
                ('author', models.ForeignKey(help_text='Author', on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Exercise',
                'verbose_name_plural': 'Exercises',
            },
        ),
        migrations.CreateModel(
            name='StudentExam',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('assigned', models.DateTimeField(verbose_name='Assignment date/time')),
                ('started', models.DateTimeField(verbose_name='Start date/time')),
                ('finished', models.DateTimeField(verbose_name='Finish date/time')),
                ('assignee', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='exams_taken', to=settings.AUTH_USER_MODEL, verbose_name='Assignee')),
                ('assigner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='exams_assigned', to=settings.AUTH_USER_MODEL, verbose_name='Assigner')),
            ],
            options={
                'verbose_name': 'Student exam',
                'verbose_name_plural': 'Student exams',
            },
        ),
        migrations.CreateModel(
            name='StudentExercise',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('answered', models.DateTimeField(help_text='Answered')),
                ('response', models.TextField(blank=True, help_text='User response', null=True)),
                ('analytics', django.contrib.postgres.fields.jsonb.JSONField(blank=True, help_text='Analytics', null=True)),
                ('result', models.IntegerField(blank=True, help_text='Result', null=True)),
                ('interval', models.FloatField(blank=True, help_text='Interval', null=True)),
                ('assignee', models.ForeignKey(help_text='Assignee', on_delete=django.db.models.deletion.CASCADE, related_name='taken_tests', to=settings.AUTH_USER_MODEL)),
                ('exam', models.ForeignKey(blank=True, help_text='Exam', null=True, on_delete=django.db.models.deletion.CASCADE, to='edu.Exam')),
                ('exercise', models.ForeignKey(help_text='Exercise', on_delete=django.db.models.deletion.CASCADE, to='edu.Exercise')),
                ('initiator', models.ForeignKey(help_text='Initiator', on_delete=django.db.models.deletion.CASCADE, related_name='initiated_tests', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Test',
                'verbose_name_plural': 'Test executions',
            },
        ),
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('kind', models.IntegerField()),
                ('default_interval', models.FloatField(help_text='Default interval')),
            ],
        ),
        migrations.AddField(
            model_name='examexercise',
            name='exercise',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='edu.Exercise', verbose_name='Exercise'),
        ),
    ]
