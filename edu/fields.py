from django.contrib.postgres.forms.jsonb import JSONField


class JSONSpecialField(JSONField):
    def to_python(self, value):
        if isinstance(value, str):
            try:
                parts = value.split()
                int_parts = [int(p) for p in parts]
                return int_parts
            except (ValueError, TypeError):
                return super().to_python(value)
        return super().to_python(value)