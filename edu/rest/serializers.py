from django.contrib.auth.models import User
from rest_framework import serializers
from .. import models as M


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'first_name', 'last_name', 'username']


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = M.UserProfile
        fields = '__all__'


class ExerciseSerializer(serializers.ModelSerializer):
    author = UserSerializer()

    class Meta:
        model = M.Exercise
        fields = '__all__'


class ExerciseAnalyticsSerializer(serializers.ModelSerializer):
    user = serializers.PrimaryKeyRelatedField(
        read_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = M.ExerciseAnalytics
        fields = '__all__'