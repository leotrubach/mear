from rest_framework import routers
from . import viewsets as V


router = routers.DefaultRouter()


router.register('exercise', V.ExerciseViewSet, base_name='exercise')
router.register('exerciseanalytics', V.ExerciseAnalyticsViewSet, base_name='exerciseanalytics')