from rest_framework import viewsets
from rest_framework import views
from rest_framework import permissions
from rest_framework import parsers
from . import serializers as S
from .. import models as M
from ..enums import UserKind


class ExerciseViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = S.ExerciseSerializer

    def get_queryset(self):
        if self.request.user.is_superuser:
            return M.Exercise.objects.all()
        profile = self.request.user.profile
        if not profile:
            return M.Exercise.objects.empty()
        if profile.kind == UserKind.teacher:
            return M.Exercise.objects.all()
        if profile.kind == UserKind.student:
            return M.Exercise.objects.filter(skill_level__lte=profile.skill_level)
        if profile.kind == UserKind.parent:
            return M.Exercise.objects.all()  # TODO: filter for parents?
        if profile.kind == UserKind.admin:
            return M.Exercise.objects.all()
        if profile.kind == UserKind.screen:
            return M.Exercise.objects.all()


class ExerciseAnalyticsViewSet(viewsets.ModelViewSet):
    serializer_class = S.ExerciseAnalyticsSerializer
    queryset = M.ExerciseAnalytics.objects.all()
    permission_classes = (permissions.IsAuthenticated, )
    parser_classes = (parsers.JSONParser, )

