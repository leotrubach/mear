from django import forms
from . import enums as E


class GenExerciseForm(forms.Form):
    kind = forms.ChoiceField(choices=E.SKILL_LEVEL_CHOICES[:14])
    length = forms.TypedChoiceField(coerce=int, choices=E.EXERCISE_LENGTH_CHOICES)