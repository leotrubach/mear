from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from . import models as M


class ReadOnlyAdmin(admin.ModelAdmin):
    readonly_fields = []

    def get_readonly_fields(self, request, obj=None):
        return [field.name for field in obj._meta.fields]

    def has_add_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False


class UserProfileInline(admin.StackedInline):
    model = M.UserProfile
    can_delete = False
    verbose_name_plural = _('Profile')


class MearUserAdmin(UserAdmin):
    inlines = (UserProfileInline, )


class ExerciseAnalyticsAdmin(ReadOnlyAdmin):
    model = M.ExerciseAnalytics
    list_display = [
        'user', 'started', 'exercise', 'interval', 'skill_level', 'result', 'answer']
    list_filter = ['user', 'started', 'interval', 'skill_level', 'result']


class EducationGroupAdmin(admin.ModelAdmin):
    model = M.EducationGroup


class EducationCenterAdmin(admin.ModelAdmin):
    model = M.EducationCenter


admin.site.unregister(User)
admin.site.register(User, MearUserAdmin)
admin.site.register(M.ExerciseAnalytics, ExerciseAnalyticsAdmin)
admin.site.register(M.EducationCenter, EducationCenterAdmin)
admin.site.register(M.EducationGroup, EducationGroupAdmin)