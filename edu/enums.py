import enum
from django.utils.translation import ugettext_lazy as _


class SkillLevel(enum.IntEnum):
    one_digit_no_formula = 1
    two_digit_no_formula_same = 2
    two_digit_no_formula_different = 3
    one_digit_partners = 4
    two_digit_partners_same = 5
    two_digit_partners_different = 6
    one_digit_friends_plus = 7
    one_digit_friends_minus = 8
    one_digit_bigfamily = 9
    one_digit_smallfamily = 10
    two_digit_friends_plus = 11
    two_digit_friends_minus = 12
    two_digit_bigfamily = 13
    two_digit_smallfamily = 14
    three_digit_no_formula = 15
    three_digit_formula = 16
    multiplication_two_by_one = 17
    multiplication_three_by_one = 18
    multiplication_four_by_one = 19


class UserKind(enum.IntEnum):
    student = 1
    teacher = 2
    parent = 3
    admin = 4
    screen = 5


USER_KIND_CHOICES = (
    (UserKind.student.value, _('Student')),
    (UserKind.teacher.value, _('Teacher')),
    (UserKind.parent.value, _('Parent')),
    (UserKind.admin.value, _('Admin')),
    (UserKind.screen.value, _('Screen'))
)

class ExerciseResult(enum.IntEnum):
    cancelled = 0
    completed_mistake = -1
    completed_correct = 1


EXERCISE_RESULT_CHOICES = (
    (ExerciseResult.cancelled.value, _('Cancelled')),
    (ExerciseResult.completed_mistake.value, _('Completed with mistake')),
    (ExerciseResult.completed_correct.value, _('Completed correctly'))
)


EXERCISE_LENGTH_CHOICES = (
    (5, 5),
    (7, 7),
    (10, 10),
    (15, 15),
    (20, 20)
)


SKILL_LEVEL_CHOICES = (
    (SkillLevel.one_digit_no_formula.value, _('One digit no formula')),
    (SkillLevel.two_digit_no_formula_same.value, _('Two digit same no formula')),
    (SkillLevel.two_digit_no_formula_different.value, _('Two digit different no formula')),
    (SkillLevel.one_digit_partners.value, _('One digit partners')),
    (SkillLevel.two_digit_partners_same.value, _('Two digit partners same')),
    (SkillLevel.two_digit_partners_different.value, _('Two digit partners different')),
    (SkillLevel.one_digit_friends_plus.value, _('One digit friends plus only')),
    (SkillLevel.one_digit_friends_minus.value, _('One digit friends minus only')),
    (SkillLevel.one_digit_bigfamily.value, _('One digit big family')),
    (SkillLevel.one_digit_smallfamily.value, _('One digit small family')),
    (SkillLevel.two_digit_friends_plus.value, _('Two digit friends plus')),
    (SkillLevel.two_digit_friends_minus.value, _('Two digits friends minus')),
    (SkillLevel.two_digit_bigfamily.value, _('Two digit big family')),
    (SkillLevel.two_digit_smallfamily.value, _('Two digit small family')),
    (SkillLevel.three_digit_no_formula.value, _('Three digit no formula')),
    (SkillLevel.three_digit_formula.value, _('Three digit formula')),
    (SkillLevel.multiplication_two_by_one.value, _('Multiplication two digit by one digit')),
    (SkillLevel.multiplication_three_by_one.value, _('Multiplication three digit by one digit')),
    (SkillLevel.multiplication_four_by_one.value, _('Multiplication four digit by one digit'))
)


SEX_CHOICES = (
    ('M', _('Male')),
    ('F', _('Female'))
)