from django.conf.urls import url, include

from . import views as V
from .rest.routers import router

urlpatterns = [
    url(r'^rest/', include(router.urls)),
    url(r'^gen-test/$', V.GenExerciseView.as_view(), name='generate-exercise'),
    url(r'^kind-list/$', V.KindList.as_view(), name='kind-list')
]
