import csv
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from os import urandom
import trans


CHARS = "234679ADEFGHJKLMNPRTUW"

kazakh = {
    u'а': u'a',  u'б': u'b',  u'в': u'v',  u'г': u'g', u'д': u'd', u'е': u'e',
    u'ё': u'yo', u'ж': u'zh', u'з': u'z',  u'и': u'i', u'й': u'y', u'к': u'k',
    u'л': u'l',  u'м': u'm',  u'н': u'n',  u'о': u'o', u'п': u'p', u'р': u'r',
    u'с': u's',  u'т': u't',  u'у': u'u',  u'ф': u'f', u'х': u'kh', u'ц': u'c',
    u'ч': u'ch', u'ш': u'sh', u'щ': u'sh', u'ъ': u'',  u'ы': u'y', u'ь': u'',
    u'э': u'e',  u'ю': u'yu', u'я': u'ya',

    u'А': u'A',  u'Б': u'B',  u'В': u'V',  u'Г': u'G', u'Д': u'D', u'Е': u'E',
    u'Ё': u'Yo', u'Ж': u'Zh', u'З': u'Z',  u'И': u'I', u'Й': u'Y', u'К': u'K',
    u'Л': u'L',  u'М': u'M',  u'Н': u'N',  u'О': u'O', u'П': u'P', u'Р': u'R',
    u'С': u'S',  u'Т': u'T',  u'У': u'U',  u'Ф': u'F', u'Х': u'Kh', u'Ц': u'C',
    u'Ч': u'Ch', u'Ш': u'Sh', u'Щ': u'Sh', u'Ъ': u'',  u'Ы': u'Y', u'Ь': u'',
    u'Э': u'E',  u'Ю': u'Yu', u'Я': u'Ya',
    u'ә': u'a', u'ғ': u'g', u'қ': u'q', u'ң': 'n', u'ө': u'o', u'ұ': u'u',
    u'ү': u'u', u'һ': u'h', u'і': u'i',
    u'Ә': u'A', u'Ғ': u'G', u'Қ': u'Q', u'Ң': 'N', u'Ө': u'O', u'Ұ': u'U',
    u'Ү': u'U', u'Һ': u'H', u'І': u'I',
}

t = trans.Trans(default_table=kazakh)

def gen_pass(length):
    return "".join(CHARS[c % len(CHARS)] for c in urandom(length))


class Command(BaseCommand):
    help = 'Imports file with pupils and outputs their passwords'

    def add_arguments(self, parser):
        parser.add_argument('filename')
        parser.add_argument('output')

    def handle(self, *args, **options):
        filename = options['filename']
        output = options['output']
        try:
            with open(filename, encoding='utf-16') as f, open(output, 'w', encoding='utf-8') as fo:
                for surname, name in csv.reader(f, dialect='excel-tab'):
                    password = gen_pass(8)
                    login = '{}.{}'.format(t(surname.lower()), t(name.lower()))
                    u = User.objects.create_user(
                        login,
                        "test@example.com",
                        password
                    )
                    u.first_name = name
                    u.last_name = surname
                    u.save()
                    fo.write('{} {} {} {}\n'.format(surname, name, login, password))
        except OSError:
            exit(1)