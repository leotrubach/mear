from django.db import models
from django.contrib.auth.models import User
from django.contrib.postgres.fields import JSONField
from django.utils.translation import ugettext_lazy as _

from . import enums as E


class EducationCenter(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('Name'))
    contacts = models.TextField(verbose_name=_('Contacts'))

    def __str__(self):
        return _('{name} ({contacts})').format(name=self.name, contacts=self.contacts)

    class Meta:
        verbose_name = _('Education center')
        verbose_name_plural = _('Education centers')


class EducationGroup(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('Name'), null=True, blank=True)
    center = models.ForeignKey(EducationCenter, verbose_name=_('Education center'))
    teacher = models.ForeignKey(
        User, limit_choices_to={'profile__kind': E.UserKind.teacher.value}, related_name='educated_groups',
        verbose_name=_('Teacher'))

    def __str__(self):
        return _('{name} (teacher: {teacher})').format(name=self.name, teacher=self.teacher)

    class Meta:
        verbose_name = _('Education group')
        verbose_name_plural = _('Education groups')


class UserProfile(models.Model):
    user = models.OneToOneField(User, related_name='profile', null=True, blank=True)
    skill_level = models.IntegerField(
        choices=E.SKILL_LEVEL_CHOICES, verbose_name=_('Skill level'), null=True, blank=True)
    kind = models.IntegerField(choices=E.USER_KIND_CHOICES, verbose_name=_('User kind'), null=True, blank=True)
    default_interval = models.FloatField(verbose_name=_('Default interval'), null=True, blank=True)
    sex = models.CharField(max_length=1, choices=E.SEX_CHOICES, verbose_name=_('Sex'), null=True, blank=True)
    education_group = models.ForeignKey(EducationGroup, verbose_name=_('Education group'), null=True, blank=True)

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')


class ExerciseAnalytics(models.Model):
    user = models.ForeignKey(User, verbose_name=_('Examined user'))
    result = models.IntegerField(
        choices=E.EXERCISE_RESULT_CHOICES, verbose_name=_('Runner result'), null=True, blank=True)
    exercise = JSONField(verbose_name=_('Exercise'), null=True, blank=True)
    interval = models.IntegerField(verbose_name=_('Interval'), null=True, blank=True)
    skill_level = models.IntegerField(verbose_name=_('Kind'), choices=E.SKILL_LEVEL_CHOICES)
    answer = models.TextField(verbose_name=_('Answer'), null=True, blank=True)
    opened = models.DateTimeField(verbose_name=_('Test form opened'), null=True, blank=True)
    requested = models.DateTimeField(verbose_name=_('Test requested'), null=True, blank=True)
    received = models.DateTimeField(verbose_name=_('Test data received'), null=True, blank=True)
    started = models.DateTimeField(verbose_name=_('Test started'), null=True, blank=True)
    cancelled = models.DateTimeField(verbose_name=_('Test cancelled'), null=True, blank=True)
    closed = models.DateTimeField(verbose_name=_('Test closed'), null=True, blank=True)
    answer_requested = models.DateTimeField(verbose_name=_('Answer requested'), null=True, blank=True)
    answer_provided = models.DateTimeField(verbose_name=_('Answer provided'), null=True, blank=True)

    class Meta:
        verbose_name = _('Exercise analytics')
        verbose_name_plural = _('Exercise analytics')






class Exercise(models.Model):  # TODO: find better name
    author = models.ForeignKey(User, verbose_name=_('Author'))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Created'))
    name = models.CharField(max_length=100, verbose_name=_('Name'))
    contents = JSONField(verbose_name=_('Contents'))
    skill_level = models.IntegerField(choices=E.SKILL_LEVEL_CHOICES, verbose_name=_('Skill level'))
    difficulty = models.FloatField(verbose_name=_('Difficulty'), null=True, blank=True)

    class Meta:
        verbose_name = _('Exercise')
        verbose_name_plural = _('Exercises')

    def __str__(self):
        return '{o.name}'.format(o=self)


class Exam(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('Name'))
    creator = models.ForeignKey(User, verbose_name=_('Creator'))
    created = models.DateTimeField(auto_now_add=True, verbose_name=_('Created'))

    class Meta:
        verbose_name = _('Exam')
        verbose_name_plural = _('Exams')

    def __str__(self):
        return '(o.name)'.format(o=self)


class StudentExercise(models.Model):
    exercise = models.ForeignKey(Exercise, verbose_name=_('Exercise'))
    exam = models.ForeignKey(Exam, verbose_name=_('Exam'), null=True, blank=True)
    initiator = models.ForeignKey(User, related_name='initiated_tests', verbose_name=_('Initiator'))
    assignee = models.ForeignKey(User, related_name='taken_tests', verbose_name=_('Assignee'))
    answered = models.DateTimeField(verbose_name=_('Answered'))
    response = models.TextField(verbose_name=_('User response'), null=True, blank=True)
    analytics = JSONField(verbose_name=_('Analytics'), null=True, blank=True)
    result = models.IntegerField(verbose_name=_('Result'), null=True, blank=True)
    interval = models.FloatField(verbose_name=_('Interval'), null=True, blank=True)

    class Meta:
        verbose_name = _('Test')
        verbose_name_plural = _('Test executions')

    def __str__(self):
        return '{o.assignee} {o.result}'.format(o=self)


class ExamExercise(models.Model):
    name = models.CharField(max_length=100, verbose_name=_('Name'))
    exam = models.ForeignKey(Exam, verbose_name=_('Exam'))
    exercise = models.ForeignKey(Exercise, verbose_name=_('Exercise'))
    rank = models.IntegerField(verbose_name=_('Rank'))

    class Meta:
        verbose_name = _('Exam exercise')
        verbose_name_plural = _('Exam exercises')

    def __str__(self):
        return '{o.rank} {o.exercise}'.format(o=self)


class StudentExam(models.Model):
    assigner = models.ForeignKey(User, verbose_name=_('Assigner'), related_name='exams_assigned')
    assignee = models.ForeignKey(User, verbose_name=_('Assignee'), related_name='exams_taken')
    assigned = models.DateTimeField(verbose_name=_('Assignment date/time'))
    started = models.DateTimeField(verbose_name=_('Start date/time'))
    finished = models.DateTimeField(verbose_name=_('Finish date/time'))

    class Meta:
        verbose_name = _('Student exam')
        verbose_name_plural = _('Student exams')

    def __str__(self):
        return '{o.assignee} {o.started}'.format(o=self)